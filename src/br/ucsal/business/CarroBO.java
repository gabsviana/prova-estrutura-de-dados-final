package br.ucsal.business;

import br.ucsal.dao.CarroDAO;
import br.ucsal.domain.Carro;
import br.ucsal.domain.Queue;
import br.ucsal.domain.Stack;

public class CarroBO {
	public static Stack<Carro> pegarTodosOsCarrosStack() {
		return CarroDAO.findAllStack();
	}

	public static Queue<Carro> pegarTodosOsCarrosQueue() {
		return CarroDAO.findAllQueue();
	}
	
	public static void salvarCarro(Carro carro) {
		if(carro.getFabricante().equalsIgnoreCase("Toyota") || carro.getFabricante().equalsIgnoreCase("T"))
			CarroDAO.saveStack(carro);
		else
			CarroDAO.saveQueue(carro);
	}
}