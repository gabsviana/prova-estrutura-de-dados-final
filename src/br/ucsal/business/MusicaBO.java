package br.ucsal.business;

import br.ucsal.dao.MusicaDAO;
import br.ucsal.domain.Musica;
import br.ucsal.domain.Queue;

public class MusicaBO {
	public static Queue<Musica> pegarTodos() {
		return MusicaDAO.findAll();
	}
	
	public static void salvarMusica(Musica musica) {
		MusicaDAO.save(musica);
	}
	
	public static void deletarMusica() {
		MusicaDAO.delete();
	}
	
	public static void passarMusica() {
		MusicaDAO.next();
	}
}
