package br.ucsal.tui;

import java.util.Random;

import br.ucsal.domain.Arvore;

public class Questao03 {
	private static Random random = new Random();
	
	public static void main(String[] args) {
		inicio();
	}

	private static void inicio() {
		Arvore arvore = new Arvore();
		for(int i = 0; i<10; i++) {
			int numero = random.nextInt()%100;
			System.out.println("Inserindo o N�mero: "+ numero);
			arvore.inserir(numero);
		}
		arvore.posOrder(arvore.getRaiz());
	}
}
