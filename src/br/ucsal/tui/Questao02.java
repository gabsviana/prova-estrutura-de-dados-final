package br.ucsal.tui;

import java.util.Scanner;

import br.ucsal.business.MusicaBO;
import br.ucsal.domain.Musica;

public class Questao02 {

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		inicio();
	}

	private static void inicio() {
		menu();
	}

	private static void menu() {
		char value;
		do {
			System.out.println("Selecione uma das Op��es abaixo!");
			System.out.println("I) INSERIR NOVA M�SICA\nP) PR�XIMA M�SICA\nF) CONSULTAR M�SICAS\nS) SAIR DO PROGRAMA");
			value = sc.next().charAt(0);
			if (value == 'S' || value == 's')
				return;
			else if (value == 'I' || value == 'i')
				inserirNovaMusica();
			else if (value == 'p' || value == 'P')
				proximaMusica();
			else if (value == 'F' || value == 'f')
				listarMusicas();
			else
				System.out.println("Escolha uma op��o v�lida!\n\n");
		} while (value != 0);
	}

	private static void proximaMusica() {
		MusicaBO.passarMusica();
	}

	private static void inserirNovaMusica() {
		System.out.println("Insira o Titulo da M�sica");
		MusicaBO.salvarMusica(new Musica(sc.next()));
		System.out.println("Musica Salva com Sucesso!");
	}

	private static void listarMusicas() {
		MusicaBO.pegarTodos().print();
	}
}
