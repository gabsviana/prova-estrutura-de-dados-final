package br.ucsal.tui;

import java.util.Scanner;

import br.ucsal.business.CarroBO;
import br.ucsal.domain.Carro;

public class Questao01 {

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		inicio();
	}

	private static void inicio() {
		menu();
	}

	private static void menu() {
		char value;
		do {
			System.out.println("Selecione uma das Op��es abaixo!");
			System.out.println("I) INSERIR NOVO CARRO\nF) LISTA A FILA\nP) LISTA A PILHA\nS) SAIR DO PROGRAMA");
			value = sc.next().charAt(0);
			if (value == 'S' || value == 's')
				return;
			else if (value == 'I' || value == 'i')
				inserirNovoCarro();
			else if (value == 'p' || value == 'P')
				exibirFila();
			else if (value == 'F' || value == 'f')
				exibirPilha();
			else
				System.out.println("Escolha uma op��o v�lida!\n\n");
		} while (value != 0);
	}

	private static void exibirPilha() {
		CarroBO.pegarTodosOsCarrosStack().print();
	}

	private static void exibirFila() {
		CarroBO.pegarTodosOsCarrosQueue().print();
	}

	private static void inserirNovoCarro() {
		System.out.println("Insira o nome do Fabricante");
		CarroBO.salvarCarro(new Carro(sc.next()));
		System.out.println("Carro Inserido com Sucesso!");
	}
}
