package br.ucsal.domain;

public class Stack<T> {
	
	private No top;
	private Integer length = 0;
	
	public void push(T object) {
		No newNode = new No(object, this.top);
		this.top = newNode;
		this.length += 1;
	}

	public void print() {
		No aux = this.top;
		while (aux != null) {
			System.out.println(aux.getObject().toString());
			aux = aux.getAnt();
		}
	}
	public class No {
		private T object;
		private No ant;

		public No(T object, No ant) {
			super();
			this.object = object;
			this.ant = ant;
		}

		public T getObject() {
			return object;
		}

		public void setObject(T object) {
			this.object = object;
		}

		public No getAnt() {
			return ant;
		}

		public void setAnt(No ant) {
			this.ant = ant;
		}
	}

}
