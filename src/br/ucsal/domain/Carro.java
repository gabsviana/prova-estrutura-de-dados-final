package br.ucsal.domain;

public class Carro {
	private String fabricante;

	public Carro(String fabricante) {
		this.fabricante = fabricante;
	}
	
	public String getFabricante() {
		return fabricante;
	}

	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}

	@Override
	public String toString() {
		return "Carro [fabricante=" + fabricante + "]";
	}
	
}
