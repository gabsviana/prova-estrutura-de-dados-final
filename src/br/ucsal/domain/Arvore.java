package br.ucsal.domain;

public class Arvore {

	private No raiz;

	public Arvore() {
		this.raiz = null;
	}

	public No getRaiz() {
		return this.raiz;
	}
	
	public No inserir(int valor) {
		No n = new No(valor);
		return inserir(n, null);
		
	}
	
	private No inserir(No novo, No pai) {

		if (pai == null)
			pai = raiz;

		if (raiz == null) {
			raiz = novo;
		} else {
			// menor
			if (novo.valor < pai.valor) {

				if (pai.obterNoEsquerdo() == null)
					pai.inserirEsquerdo(novo);
				else
					inserir(novo, pai.obterNoEsquerdo());

			} else {

				if (pai.obterNoDireito() == null)
					pai.inserirDireito(novo);
				else
					inserir(novo, pai.obterNoDireito());
			}
		}

		return novo;

	}
	
	 public void posOrder(No atual) {
		    if (atual != null) {
		      posOrder(atual.esquerdo);
		      posOrder(atual.direito);
		      System.out.print(atual.valor + " ");
		    }
		  } 
	

	public class No {

		No esquerdo;
		No direito;
		int valor;
		No pai;

		public No(int valor) {
			this.valor = valor;
		}

		public No(int valor, No pai) {
			this.valor = valor;
			this.pai = pai;
		}

		public void inserirValor(int valor) {
			this.valor = valor;
		}

		public int obterValor() {
			return this.valor;
		}

		public void inserirEsquerdo(No esquerdo) {
			this.esquerdo = esquerdo;
			if (esquerdo != null)
				esquerdo.pai = this;
		}

		public void inserirDireito(No direito) {
			this.direito = direito;
			if (direito != null)
				direito.pai = this;
		}

		public No obterNoEsquerdo() {
			return this.esquerdo;
		}

		public No obterNoDireito() {
			return this.direito;
		}

		public String toString() {
			return Integer.toString(this.valor);
		}

		public void inseriPai(No pai) {
			this.pai = pai;
		}

		public No obterPai() {
			return this.pai;
		}

	}
}
