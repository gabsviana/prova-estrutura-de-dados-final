package br.ucsal.domain;

public class Queue<T> {
	private No head;
	private No tail;
	private int size;

	public int length() {
		return this.size;
	}
	
	public T get(int index) {
		if (!(index >= 0 && index < this.size))
			throw new IllegalArgumentException("Erro");
		No aux = this.head;
		for (int i = 0; i < index; i++)
			aux = aux.getProx();
		return aux.getObject();
	}

	public void add(T object) {
		if (this.size == 0) {
			No newNode = new No(this.head, object);
			this.head = newNode;
			if (size == 0) {
				this.tail = this.head;
			}
		} else {
			No newNode = new No(object);
			this.tail.setProx(newNode);
			this.tail = newNode;
		}
		this.size += 1;
	}
	
	public void remove() {
		this.head = this.head.getProx();
		this.size--;
		if(this.size == 0)
			this.tail = null;
	}
	
	public void print() {
		for (int i = 0; i < size; i++)
			System.out.println(get(i).toString());
	}

	public class No {
		private No prox;
		private T object;

		public No(No prox, T object) {

			this.prox = prox;
			this.object = object;
		}

		public No(T object) {
			this.object = object;
		}

		public No getProx() {
			return prox;
		}

		public void setProx(No prox) {
			this.prox = prox;
		}

		public T getObject() {
			return object;
		}
	}

}
