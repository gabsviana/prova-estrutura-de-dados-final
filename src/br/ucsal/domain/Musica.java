package br.ucsal.domain;

public class Musica {
	private String titulo;

	public Musica(String titulo) {
		super();
		this.titulo = titulo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	@Override
	public String toString() {
		return "Musica [titulo=" + titulo + "]";
	}
}
