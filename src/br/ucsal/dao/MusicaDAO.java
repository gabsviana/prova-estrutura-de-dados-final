package br.ucsal.dao;

import br.ucsal.domain.Musica;
import br.ucsal.domain.Queue;

public class MusicaDAO {
	private static Queue<Musica> musicas = new Queue<>();
	
	public static Queue<Musica> findAll() {
		return musicas;
	}

	public static void save(Musica musica) {
		musicas.add(musica);
	}
	
	public static void delete() {
		musicas.remove();
	}

	public static void next() {
		Musica musica = musicas.get(musicas.length()-1);
		System.out.println("Tocando M�sica =["+ musica.toString() +"]");
		musicas.remove();
		System.out.println("PASSANDO PARA A PR�XIMA M�SICA");
		musicas.add(musica);
	}
}