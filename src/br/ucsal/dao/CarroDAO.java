package br.ucsal.dao;

import br.ucsal.domain.Carro;
import br.ucsal.domain.Queue;
import br.ucsal.domain.Stack;

public class CarroDAO {
	private static Stack<Carro> carrosPilha = new Stack<>();
	private static Queue<Carro> carrosFila = new Queue<>();
	
	public static Stack<Carro> findAllStack() {
		return carrosPilha;
	}
	
	public static Queue<Carro> findAllQueue() {
		return carrosFila;
	}
	public static void saveStack(Carro carro) {
		carrosPilha.push(carro);
	}
	public static void saveQueue(Carro carro) {
		carrosFila.add(carro);
	}
}
